
public class Memento {
    Cell PlayerLocation, SheepLocation, WolfLocation, ShepherdLocation;

    public Memento(Cell PlayerLoc, Cell SheepLoc, Cell WolfLoc, Cell ShepherdLoc){
        PlayerLocation = PlayerLoc;
        SheepLocation = SheepLoc;
        WolfLocation = WolfLoc;
        ShepherdLocation = ShepherdLoc;
    }
    public Cell getPlayer(){
        return PlayerLocation;
    }
    public Cell getSheep(){
        return SheepLocation;
    }
    public Cell getWolf(){
        return WolfLocation;
    }
    public Cell getSheperd(){
        return ShepherdLocation;
    }
}
