import java.awt.*;
import java.util.List;
import java.util.Optional;
import bos.*;

public class Block extends Character {

    public Block(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(new Color(139,69,19));
    }

    @Override
    public void paint(Graphics g){
        if(display.isPresent()) {
            g.setColor(display.get());
            g.fillRect(location.x + location.width, location.y + location.height, location.width, location.height);
        }
    }
}
